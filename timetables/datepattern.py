import simpleparse.parser

# This is an EBNF (as understood by simpleparse) for Timetables date time
# pattern strings.
_date_time_pattern_grammar = r"""
root := pattern_list
pattern_list := WS, pattern, (WS, ";", WS, pattern)*, WS
pattern := (term, WS)?, weeks, WS, timeslot, (WS, timeslot)*

timeslot := days, WS, time

time := time_range / NUM
time_range := NUM, WS, "-", WS, NUM

days := day_range / day_list
day_range := day, WS, "-", WS, day
day_list := day, (WS, (",", WS)?, day)*
day := MON / TUE / WED / THU / FRI / SAT / SUN
MON := c"mo" / c"m"
TUE := c"tu"
WED := c"we" / c"w"
THU := c"th"
FRI := c"fr" / c"f"
SAT := c"sa"
SUN := c"su"

weeks := week_range / week_list
week_list := NUM, (WS, ",", WS, NUM)*
week_range := NUM, WS, "-", WS, NUM

NUM := [0-9]+

term := MICHAELMAS / LENT / EASTER
MICHAELMAS := c"mi"
LENT := c"le"
EASTER := c"ea"

# whitespace
<WS> := " "*
"""

# Compile the grammar into a parser
parser = simpleparse.parser.Parser(_date_time_pattern_grammar)

