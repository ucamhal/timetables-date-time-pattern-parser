import unittest
from timetables import datepattern

class ParserTest(unittest.TestCase):

    # Define some testcases to run. First tuple value is the production to 
    # match with. The second is the string to match.
    testcases = [
        ("WS", " "),
        ("WS", ""),
        ("term", "Mi"),
        ("term", "Ea"),
        ("term", "Le"),
        ("NUM", "1"),
        ("NUM", "23"),
        ("weeks", "1,2,3"),
        ("weeks", "1, 2 ,3 , 6"),
        ("weeks", "1-6"),
        ("weeks", "1 - 6"),
        ("day", "mo"),
        ("day", "tu"),
        ("day", "w"),
        ("day", "Th"),
        ("day", "F"),
        ("day", "sU"),
        ("day", "sa"),
        ("day_list", "m,tu,w,th,fr"),
        ("day_range", "Tu-su"),
        ("days", "Mo-Fr"),
        ("days", "Mo, Tu , w, Th, f, sA,  su"),
        ("days", "MoTuwThfsAsu"),
        ("days", "MoTu,w,ThfsA,su"),
        ("time", "10"),
        ("time", "9"),
        ("time", "9-10"),
        ("time", "1 - 6"),
        ("timeslot", "Mo,Tu,We 10"),
        ("timeslot", "Th-Su10"),
        ("pattern", "Mi 1-5 Th-Su 10"),
        ("pattern", "Mi 1-5 Th-Su 10 Mo 3-5"),
        ("pattern", "Mi 1-5 Th-Su 10 Mo 3-5"),
        ("pattern_list", "Le1-2 Th 10 ; Le3-8 Th 9 ; Le4-7 W 9"),
        ("pattern_list", "Le3-8 W-Th 10"),
    ]

    def test_testcases(self):
        """Checks the date time pattern grammar by running a list of test cases.
        """
        parser = datepattern.parser

        # Run each test case
        for production, testcase in ParserTest.testcases:
            try:
                # Try to parse each testcase value with the associated 
                # production rule.
                success, children, nextchar = parser.parse(testcase,
                        production=production)

                # The value must have matched the production rule
                self.assertTrue(success)
                # The entire string must have been consumed by the parser
                self.assertEqual(len(testcase), nextchar)
            except:
                print ""
                print "production: %s, testcase: %s" % (production, testcase)
                raise

if __name__ == '__main__':
    unittest.main()
