pattern_list = pattern+

pattern = WS (term:term week:week ts:ts) WS ";"? {return {term: term, weeks: week, timeslots: ts}}

/*    WEEK    */
week = (week:(week_range / week_list) WS?) {return week}
week_range = left:NUM "-" right:NUM {return{type: "range", from: left, to: right}}
week_list = first:NUM rest:week_rest* {return { type: "list", items: [first].concat(rest)}}
week_rest = "," num:NUM { return num }


/* NUM, (WS, ",", WS, NUM)*
(week_sep:NUM ("," WS?)?)*/

/*  TIMESLOT  */
ts = ts:timeslots+ {return ts}
timeslots = (timeslot:(timeslot / timeslot_range) (" " WS?)?) {return timeslot}
timeslot = (day:day+ WS? time:time) { return {days: {type: "list", items: day} , time: time}}
timeslot_range = (from:day "-" to:day WS? time:time) { return { days: {type: "range", from: from, to: to} , time: time}}

/*    DAY     */
day = day:(MON / TUE / WED / THU / FRI / SAT / SUN) {return day}

MON = ("mo"i / "m"i) { return "Monday"}
TUE = ("tu"i) { return "Tuesday"}
WED = ("we"i / "w"i) { return "Wednesday"}
THU = ("th"i) { return "Thursday"}
FRI = ("fr"i / "f"i) { return "Friday"}
SAT = ("sa"i) { return "Saturday"}
SUN = ("su"i) { return "Sunday"}

/*    TERM    */
term = (term:(MI / LE / EA) WS?) {return term}
MI = ("Mi"i) { return "Michealmas" }
LE = ("Le"i) { return "Lent" }
EA = ("Ea"i) { return "Easter" }

/*    TIME    */
time = time:(time_range / only_start_time) {return time}

time_range = from:tr_hourmin "-" to:tr_hourmin {return {from: from, to:to}}
tr_hourmin = hourdigits:NUM "." mindigits:NUM { return {hour: hourdigits, min: mindigits}}

only_start_time = hourmin:os_hourmin { return hourmin }
os_hourmin = hourdigits:NUM "." mindigits:NUM { return {from: {hour: hourdigits, min: mindigits}, to: {hour: hourdigits + 1, min: mindigits}} }

/*    NUM     */
NUM = digits:[0-9]+ { return parseInt(digits.join("")) }

/* WHITESPACE */
WS = (ws:" "i*)